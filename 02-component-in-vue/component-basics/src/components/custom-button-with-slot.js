import Vue from 'vue';

Vue.component('custom-button-slot', {
  template: `<button class="custom-button">
    <slot><span class="default-text">Default button text</span></slot>
  </button>`
});
