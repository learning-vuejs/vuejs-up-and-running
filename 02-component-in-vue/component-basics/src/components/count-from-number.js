import Vue from 'vue';

Vue.component('count-from-number', {
  template: '<p>The number is {{ number }}</p>',
  props: {
    number: {
      type: Number,
      required: true
    }
  },
  mounted() {
    setInterval(() => {
      this.$emit('update:number', this.number + 1);
    }, 1000);
  }
});

Vue.component('count-from-number2', {
  template: '<p>The second number is {{ localNumber }}</p>',
  props: {
    number: {
      type: Number,
      required: true
    }
  },
  computed: {
    localNumber: {
      get() {
        return this.number;
      },
      set(value) {
        this.$emit('update:number', value);
      }
    }
  },
  mounted() {
    setInterval(() => {
      this.localNumber++;
    }, 1000);
  }
});

Vue.component('count-from-number3', {
  template: '<p>The another number is {{ number }}</p>',
  props: {
    initialNumber: {
      type: Number,
      required: true
    }
  },
  data() {
    return {
      number: this.initialNumber
    };
  },
  mounted() {
    setInterval(() => {
      this.number++;
    }, 1000);
  }
});
