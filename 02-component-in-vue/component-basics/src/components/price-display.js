import Vue from 'vue';

Vue.component('price-display', {
  template: `<div>{{price}} {{unit}}</div>`,
  props: {
    price: Number,
    unit: String
  }
});

Vue.component('price-display2', {
  template: `<div>{{price}} {{unit}}</div>`,
  props: {
    price: {
      type: Number,
      required: true
    },
    unit: {
      type: String,
      default: '$'
    }
  }
});

Vue.component('price-display3', {
  template: `<div>{{price}} {{unit}}</div>`,
  props: {
    price: {
      type: Number,
      required: true,
      validator(value) {
        return value >= 0;
      }
    },
    unit: {
      type: String,
      default: '€'
    }
  }
});
