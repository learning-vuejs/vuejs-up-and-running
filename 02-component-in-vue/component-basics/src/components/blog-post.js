import Vue from 'vue';

Vue.component('blog-post', {
  template: `
  <section class="blog-post">
    <header>
      <slot name="header"></slot>
      <p>Post by {{ author }}</p>
    </header>
  
    <main>
      <slot></slot>
    </main>
  </section>
  `,
  props: ['author']
});
