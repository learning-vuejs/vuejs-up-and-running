import Vue from 'vue';

Vue.component('custom-event', {
  template: `<button @click="handleClick">Clicked {{ clicks }} times</button>`,
  data: () => ({
    clicks: 0
  }),
  methods: {
    handleClick() {
      this.clicks++;
      this.$emit('count', this.clicks);
    }
  }
});
