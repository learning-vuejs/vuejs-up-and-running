import Vue from 'vue';

Vue.component('color-preview', {
  template: '<div class="color-preview" :style="style">{{ color }}</div>',
  props: ['color'],
  computed: {
    style() {
      return {
        backgroundColor: this.color,
        color: '#fff'
      };
    }
  }
});
